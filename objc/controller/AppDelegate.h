//
//  AppDelegate.h
//  objc
//
//  Created by Vilar da Camara Neto on 08/07/19.
//  Copyright © 2019 Vilar da Camara Neto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

