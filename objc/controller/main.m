//
//  main.m
//  objc
//
//  Created by Vilar da Camara Neto on 08/07/19.
//  Copyright © 2019 Vilar da Camara Neto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
