//
//  ViewController.m
//  objc
//
//  Created by Vilar da Camara Neto on 08/07/19.
//  Copyright © 2019 Vilar da Camara Neto. All rights reserved.
//

#import "ViewController.h"
#import "../model/Movie.h"


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [Movie.all count];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Movie" forIndexPath:indexPath];
    Movie *movie = [Movie.all objectAtIndex:indexPath.row];

    cell.textLabel.text = movie.portugueseTitle ? movie.portugueseTitle : movie.title;
    cell.detailTextLabel.text = movie.description;

    return cell;
}

@end
