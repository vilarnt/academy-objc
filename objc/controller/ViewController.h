//
//  ViewController.h
//  objc
//
//  Created by Vilar da Camara Neto on 08/07/19.
//  Copyright © 2019 Vilar da Camara Neto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

