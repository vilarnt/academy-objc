//
//  Movie.m
//  objc
//
//  Created by Vilar da Camara Neto on 09/07/19.
//  Copyright © 2019 Vilar da Camara Neto. All rights reserved.
//

#import "Movie.h"

@implementation Movie


static NSArray *_all = nil;


+ (NSArray *)all {
    if (_all == nil) {
        NSMutableArray *movies = [[NSMutableArray alloc] init];
        Movie *movie;

        movie = [[Movie alloc] initWithTitle:@"(500) Days of Summer" portugueseTitle:@"(500) Dias com Ela" genre:COMEDY duration:@5700 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"27 Dresses" portugueseTitle:@"Vestida para Casar" genre:COMEDY duration:@6660 year:nil];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"A Dangerous Method" portugueseTitle:@"Um Método Perigoso" genre:BIOGRAPHY duration:@5940 year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"A Serious Man" portugueseTitle:@"Um Homem Sério" genre:COMEDY duration:@6360 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Across the Universe" portugueseTitle:nil genre:DRAMA duration:@7980 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Beginners, así se siente el amor" portugueseTitle:@"Toda Forma de Amor" genre:COMEDY duration:@6300 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Dear John" portugueseTitle:@"Querido John" genre:DRAMA duration:@6480 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Enchanted" portugueseTitle:@"Encantada" genre:ANIMATION duration:@6420 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Fireproof" portugueseTitle:@"À Prova de Fogo" genre:DRAMA duration:nil year:nil];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Four Christmases" portugueseTitle:@"Surpresas do Amor" genre:COMEDY duration:@5280 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Ghosts of Girlfriends Past" portugueseTitle:@"Minhas Adoráveis Ex-Namoradas" genre:COMEDY duration:nil year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Gnomeo and Juliet" portugueseTitle:nil genre:ANIMATION duration:nil year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Going the Distance" portugueseTitle:@"Amor à Distância" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Good Luck Chuck" portugueseTitle:@"Maldita Sorte" genre:COMEDY duration:@6060 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"He's Just Not That Into You" portugueseTitle:@"Ele Não Está Tão a Fim de Você" genre:COMEDY duration:@7740 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"High School Musical 3: Senior Year" portugueseTitle:@"High School Musical 3: Ano da Formatura" genre:COMEDY duration:@6720 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"I Love You Phillip Morris" portugueseTitle:@"O Golpista do Ano" genre:BIOGRAPHY duration:@6120 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"It's Complicated" portugueseTitle:@"Simplesmente Complicado" genre:COMEDY duration:nil year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Jane Eyre" portugueseTitle:nil genre:DRAMA duration:@7200 year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Just Wright - In diesem Spiel zählt jeder Treffer" portugueseTitle:@"Jogada Certa" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Killers" portugueseTitle:@"Par Perfeito" genre:ACTION duration:@6000 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Knocked Up" portugueseTitle:@"Ligeiramente Grávidos" genre:COMEDY duration:@7740 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Leap Year" portugueseTitle:@"Casa Comigo?" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Letters to Juliet" portugueseTitle:@"Cartas para Julieta" genre:ADVENTURE duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"License to Wed" portugueseTitle:@"Licença para Casar" genre:COMEDY duration:@5460 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Life as We Know It" portugueseTitle:@"Juntos Pelo Acaso" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Love & Other Drugs" portugueseTitle:@"Amor e Outras Drogas" genre:COMEDY duration:@6720 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Love Happens" portugueseTitle:@"O Amor Acontece" genre:DRAMA duration:nil year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Made of Honor" portugueseTitle:@"O Melhor Amigo da Noiva" genre:COMEDY duration:@6060 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Mamma Mia! O Filme" portugueseTitle:nil genre:COMEDY duration:@6480 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Marley and Me" portugueseTitle:@"Marley & Eu" genre:COMEDY duration:nil year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Midnight in Paris" portugueseTitle:@"Meia-Noite em Paris" genre:COMEDY duration:nil year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Miss Pettigrew Lives for a Day" portugueseTitle:@"A Vida num Só Dia" genre:COMEDY duration:@5520 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Monte Carlo" portugueseTitle:nil genre:ADVENTURE duration:nil year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Music and Lyrics" portugueseTitle:@"Letra e Música" genre:COMEDY duration:@5700 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"My Week with Marilyn" portugueseTitle:@"Sete Dias com Marilyn" genre:BIOGRAPHY duration:@5940 year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"New Year's Eve" portugueseTitle:@"Noite de Ano Novo" genre:COMEDY duration:@6780 year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Nick and Norah's Infinite Playlist" portugueseTitle:@"Nick & Norah: Uma Noite de Amor e Música" genre:COMEDY duration:@5400 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"No Reservations" portugueseTitle:@"Sem Reservas" genre:COMEDY duration:@6240 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Not Easily Broken" portugueseTitle:@"Ponto de Decisão" genre:DRAMA duration:@5940 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"One Day (Siempre el mismo día)" portugueseTitle:@"Um Dia" genre:DRAMA duration:@6420 year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Our Family Wedding" portugueseTitle:@"Nossa União, Muita Confusão" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Over Her Dead Body" portugueseTitle:@"Nem por Cima do Meu Cadáver" genre:COMEDY duration:@5700 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"P.S. I Love You" portugueseTitle:@"P.S. Eu te Amo" genre:DRAMA duration:@7560 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Penelope" portugueseTitle:nil genre:COMEDY duration:@6240 year:@2006];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Rachel Getting Married" portugueseTitle:@"O Casamento de Rachel" genre:DRAMA duration:@6780 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Remember Me" portugueseTitle:@"Viva - A Vida é uma Festa" genre:ANIMATION duration:nil year:@2017];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Sex and the City" portugueseTitle:nil genre:COMEDY duration:@1800 year:@1998];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Sex and the City 2" portugueseTitle:nil genre:COMEDY duration:@8760 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"She's Out of My League" portugueseTitle:@"Ela é Demais Pra Mim" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Something Borrowed" portugueseTitle:@"Muito Bem Acompanhada" genre:COMEDY duration:@5400 year:@2005];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Tangled" portugueseTitle:@"Enrolados" genre:ANIMATION duration:@6000 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Back-up Plan" portugueseTitle:@"Plano B" genre:COMEDY duration:@6240 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Curious Case of Benjamin Button" portugueseTitle:@"O Curioso Caso de Benjamin Button" genre:DRAMA duration:@9960 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Duchess" portugueseTitle:@"A Duquesa" genre:BIOGRAPHY duration:@6600 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Heartbreak Kid" portugueseTitle:@"Antes Só do que Mal Casado" genre:COMEDY duration:@6960 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Invention of Lying" portugueseTitle:@"O Primeiro Mentiroso" genre:COMEDY duration:@6000 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Proposal" portugueseTitle:@"A Proposta" genre:COMEDY duration:@6480 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Time Traveler's Wife" portugueseTitle:@"Te Amarei Para Sempre" genre:DRAMA duration:@6420 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Twilight Saga: New Moon" portugueseTitle:@"A Saga Crepúsculo: Lua Nova" genre:ADVENTURE duration:@7800 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"The Ugly Truth" portugueseTitle:@"A Verdade Nua e Crua" genre:COMEDY duration:@5760 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"La saga Twilight: La fascination" portugueseTitle:@"Crepúsculo" genre:DRAMA duration:@7320 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Twilight: Breaking Dawn Part 1: The PSA Part 1" portugueseTitle:nil genre:SHORT duration:nil year:@2012];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Tyler Perry's Why Did I Get Married?" portugueseTitle:@"Por Que Eu Me Casei?" genre:COMEDY duration:nil year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Valentine's Day" portugueseTitle:@"Idas e Vindas do Amor" genre:COMEDY duration:@7500 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Wall-E" portugueseTitle:@"WALL·E" genre:ANIMATION duration:@5880 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Waiting for Forever" portugueseTitle:@"Esperar para Sempre" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Waitress" portugueseTitle:@"Garçonete" genre:COMEDY duration:@6480 year:@2007];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Water for Elephants" portugueseTitle:@"Água para Elefantes" genre:DRAMA duration:nil year:@2011];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"What Happens in Vegas" portugueseTitle:@"Jogo de Amor em Las Vegas" genre:COMEDY duration:@5940 year:@2008];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"When in Rome - Fünf Männer sind vier zuviel" portugueseTitle:@"Quando em Roma" genre:COMEDY duration:@5460 year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"You Will Meet a Tall Dark Stranger" portugueseTitle:@"Você Vai Conhecer o Homem dos Seus Sonhos" genre:COMEDY duration:nil year:@2010];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Youth in Revolt" portugueseTitle:@"Rebelde com Causa" genre:COMEDY duration:@5400 year:@2009];
        [movies addObject:movie];
        movie = [[Movie alloc] initWithTitle:@"Zack and Miri Make a Porno" portugueseTitle:@"Pagando Bem, que Mal Tem?" genre:COMEDY duration:@6060 year:@2008];
        [movies addObject:movie];

        _all = movies;
    }

    return _all;
}


- (instancetype)initWithTitle:(NSString *)title portugueseTitle:(NSString *)portugueseTitle genre:(MovieGenre)genre duration:(NSNumber *)duration year:(NSNumber *)year {
    self = [super init];

    if (self) {
        self.title = title;
        self.portugueseTitle = portugueseTitle;
        self.genre = genre;
        self.duration = duration;
        self.year = year;
    }

    return self;
}


- (NSString *)description {
    NSString *description = @"";

    if (self.year) {
        NSString *yearDescription = [NSString stringWithFormat:@"Year: %@", self.year];
        description = [description stringByAppendingString:yearDescription];
    } else {
        description = [description stringByAppendingString:@"Year: unknown"];
    }

    if (self.duration) {
        NSString *durationDescription = [NSString stringWithFormat:@", duration: %@ seconds", self.duration];
        description = [description stringByAppendingString:durationDescription];
    } else {
        description = [description stringByAppendingString:@", duration: unknown"];
    }

    return description;
}


@end
