//
//  Movie.h
//  objc
//
//  Created by Vilar da Camara Neto on 09/07/19.
//  Copyright © 2019 Vilar da Camara Neto. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    ACTION,
    ADVENTURE,
    ANIMATION,
    BIOGRAPHY,
    COMEDY,
    DRAMA,
    SHORT,
} MovieGenre;


@interface Movie : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) NSString *portugueseTitle;
@property (nonatomic) MovieGenre genre;
@property (nonatomic) NSNumber *duration;
@property (nonatomic) NSNumber *year;

+ (NSArray *)all;

- (instancetype)initWithTitle:(NSString *)title portugueseTitle:(NSString *)portugueseTitle genre:(MovieGenre)genre duration:(NSNumber *)duration year:(NSNumber *)year;
- (NSString *)description;

@end
